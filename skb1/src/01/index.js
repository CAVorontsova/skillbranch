import express from 'express';
import canonize from './canonize';

const app = express();
app.get('/',(req, res) => {
	/*console.log(req.query);*/
	const username = canonize(req.query.url);
	res.json({
		url: req.query.url,
		username,
	})
	/*res.send('Hello World!')*/
});
app.listen(3000, function() {
	console.log('Example app listening on port 3000!');
});




