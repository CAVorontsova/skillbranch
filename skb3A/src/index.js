import express from 'express';
import cors from 'cors';
import fetch from 'isomorphic-fetch';


const pcUrl = 'https://gist.githubusercontent.com/isuvorov/ce6b8d87983611482aac89f6d7bc0037/raw/pc.json';
let pc = {};
fetch(pcUrl)
  .then(async (res) => {
    pc = await res.json();
  })
  .catch(err => {
    console.log('Error: ', err);
  });

const app = express();
app.use(cors());


app.get('/', (req, res) => {
  res.json(pc);
});

app.get('/volumes', (req, res) => { 
  if (pc.hdd) { 
    const resultVolumes = {};
    console.log(resultVolumes);
    pc.hdd.map((item) => { 
      console.log(item);
      if (resultVolumes[item.volume]) {
        console.log(item.volume)
        resultVolumes[item.volume] += item.size;
      } else {
        resultVolumes[item.volume] = item.size;
        console.log(item.volume)
      }
    });


    console.log(resultVolumes, typeof(resultVolumes), typeof(pc));

 
    Object.keys(resultVolumes).forEach( (key) => { 
      console.log(resultVolumes); 
      console.log(key);
     
      resultVolumes[key] += 'B';
      console.log(resultVolumes[key]);
})
res.json(resultVolumes);
  } else {
    
    res.status(404).send('Not Found');
  }
});


app.get('/:param1', (req, res) => { 
  const reqParams = req.params;
  if (pc[reqParams.param1] !== undefined) {
    res.json(pc[reqParams.param1]);
    console.log('pc[hdd][length]: ' + pc[reqParams.hdd][reqParams.length]);
    console.log('reqParams: ' + reqParams);
  } else {
    res.status(404).send('Not Found');
  };
});


app.get('/:param1/:param2', (req, res) => { 
  const reqParams = req.params;
  if (pc[reqParams.param1] !== undefined && pc[reqParams.param1][reqParams.param2] !== undefined) {
    res.json(pc[reqParams.param1][reqParams.param2]);
    console.log(pc[reqParams.hdd][reqParams.length]);
    console.log(reqParams);
  } else {
    res.status(404).send('Not Found');
  };
});

app.get('/:param1/:param2/:param3', (req, res) => {
  const reqParams = req.params;
  if (pc[reqParams.param1] !== undefined && pc[reqParams.param1][reqParams.param2] !== undefined && pc[reqParams.param1][reqParams.param2][reqParams.param3]) {
    res.json(pc[reqParams.param1][reqParams.param2][reqParams.param3]);
  } else {
    res.status(404).send('Not Found');
  };
});





app.listen(3000, ()=>{
	console.log('start app on port 3000!')
});